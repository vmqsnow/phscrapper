import 'package:flutter/material.dart';
import 'package:pornhub_scrapp/data/models/video.dart';
import 'package:video_player/video_player.dart';

class VideoScreen extends StatefulWidget {
  const VideoScreen({Key? key, required this.video}) : super(key: key);
  final Video video;

  @override
  _VideoScreenState createState() => _VideoScreenState();
}

class _VideoScreenState extends State<VideoScreen> {
  VideoPlayerController? controller;

  @override
  void initState() {
    controller = VideoPlayerController.network(widget.video.videoUrl!)
      ..initialize().then((_) {
        controller!.setVolume(0);
        setState(() {});
      });
    super.initState();
  }

  @override
  void dispose() {
    if (controller != null) {
      controller!.dispose();
    }

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.video.title),
      ),
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              children: [
                Opacity(
                  opacity: 1,
                  child: SizedBox(
                    height: 100,
                    width: 100,
                    child: Image.network(widget.video.imageUrl),
                  ),
                ),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text('Id:${widget.video.id}'),
                        Text('Title:${widget.video.title}'),
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
          if (controller?.value.isInitialized ?? false)
            AspectRatio(
              aspectRatio: controller!.value.aspectRatio,
              child: VideoPlayer(controller!),
            ),
          if (controller != null)
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: IconButton(
                    icon: const Icon(Icons.fast_rewind),
                    onPressed: () {
                      if (controller!.value.isPlaying) {
                        Duration currentPosition = controller!.value.position;
                        Duration targetPosition = currentPosition - const Duration(seconds: 10);
                        controller!.seekTo(targetPosition);
                      }
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: IconButton(
                    icon: Icon(controller!.value.isPlaying ? Icons.pause : Icons.play_arrow),
                    onPressed: () {
                      if (controller!.value.isPlaying) {
                        controller!.pause();
                      } else {
                        controller!.play();
                      }
                      setState(() {});
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: IconButton(
                    icon: const Icon(Icons.fast_forward),
                    onPressed: () {
                      if (controller!.value.isPlaying) {
                        Duration currentPosition = controller!.value.position;
                        Duration targetPosition = currentPosition + const Duration(seconds: 10);
                        controller!.seekTo(targetPosition);
                      }
                    },
                  ),
                )
              ],
            )
        ],
      ),
    );
  }
}
