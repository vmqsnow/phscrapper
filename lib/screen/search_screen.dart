import 'package:flutter/material.dart';
import 'package:flutter_search_bar/flutter_search_bar.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';
import 'package:pornhub_scrapp/data/models/video.dart';
import 'package:pornhub_scrapp/data/repo/search_repo.dart';
import 'package:pornhub_scrapp/screen/video_screen.dart';

class SearchScreen extends StatefulWidget {
  const SearchScreen({Key? key}) : super(key: key);

  @override
  _SearchScreenState createState() => _SearchScreenState();
}

class _SearchScreenState extends State<SearchScreen> {
  // List<Video> videos = [];
  String search = '';
  String title = 'Videos';

  late SearchBar searchBar;
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  void onSubmitted(String value) {
    title = value.toUpperCase();
    search = value;
    setState(() {});
  }

  Future<void> onCleared() async {
    title = 'Videos';
    search = '';
    setState(() {});
  }

  @override
  void initState() {
    searchBar = SearchBar(
      inBar: false,
      buildDefaultAppBar: (context) {
        return AppBar(title: Text(title), actions: [searchBar.getSearchAction(context)]);
      },
      setState: setState,
      onSubmitted: onSubmitted,
      onCleared: onCleared,
    );

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: searchBar.build(context),
      key: _scaffoldKey,
      body: CharacterListView(key: ValueKey(search), search: search),
    );
  }
}

class VideoList extends StatelessWidget {
  const VideoList({Key? key, required this.videos}) : super(key: key);
  final List<Video> videos;

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        itemCount: videos.length,
        itemBuilder: (context, index) {
          final video = videos[index];
          return ListTile(
            leading: Opacity(opacity: 1, child: Image.network(video.imageUrl)),
            title: Text(video.title),
            subtitle: Text(video.id),
            onTap: () async {
              final navigator = Navigator.of(context);
              final newVideo = await SearchRepository.getVideo(video);
              navigator.push(MaterialPageRoute(builder: (context) => VideoScreen(video: newVideo)));
            },
          );
        });
  }
}

class CharacterListView extends StatefulWidget {
  final String search;

  const CharacterListView({super.key, required this.search});

  @override
  _CharacterListViewState createState() => _CharacterListViewState();
}

class _CharacterListViewState extends State<CharacterListView> {
  late final pageSize;

  final PagingController<int, Video> _pagingController = PagingController(firstPageKey: 1);

  @override
  void initState() {
    _pagingController.addPageRequestListener((pageKey) {
      _fetchPage(pageKey);
    });
    if (widget.search != '') {
      pageSize = 24;
    } else {
      pageSize = 46;
    }
    super.initState();
  }

  Future<void> _fetchPage(int pageKey) async {
    try {
      final newItems = await SearchRepository.query(query: widget.search, page: pageKey);
      final isLastPage = newItems.length < pageSize;
      if (isLastPage) {
        _pagingController.appendLastPage(newItems);
      } else {
        final nextPageKey = pageKey + 1;
        _pagingController.appendPage(newItems, nextPageKey);
      }
    } catch (error) {
      _pagingController.error = error;
    }
  }

  @override
  Widget build(BuildContext context) => PagedListView<int, Video>(
        pagingController: _pagingController,
        builderDelegate: PagedChildBuilderDelegate<Video>(
          itemBuilder: (context, video, index) => ListTile(
            leading: Opacity(opacity: 1, child: Image.network(video.imageUrl)),
            title: Text(video.title),
            subtitle: Text(video.id),
            onTap: () async {
              final navigator = Navigator.of(context);
              final newVideo = await SearchRepository.getVideo(video);
              navigator.push(MaterialPageRoute(builder: (context) => VideoScreen(video: newVideo)));
            },
          ),
        ),
      );

  @override
  void dispose() {
    _pagingController.dispose();
    super.dispose();
  }
}
