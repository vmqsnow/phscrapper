class Video {
  final String title;
  final String imageUrl;
  final String id;
  final String? videoUrl;

  Video({required this.title, required this.imageUrl, required this.id, this.videoUrl});

  Video copyWith({String? videoUrl}) {
    return Video(
      id: id,
      title: title,
      imageUrl: imageUrl,
      videoUrl: videoUrl ?? this.videoUrl,
    );
  }
}
