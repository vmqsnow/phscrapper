import 'dart:developer';

import 'package:flutter_js/flutter_js.dart';
import 'package:html/parser.dart' as parser;
import 'package:http/http.dart' as http;
import 'package:pornhub_scrapp/data/models/video.dart';

class SearchRepository {
  static Future<List<Video>> query({String? query, int? page}) async {
    final List<Video> videos = [];
    final Map<String, String> parameters = {};
    if (query != null && query != '') {
      parameters['search'] = query;
    }
    if (page != null && page > 1) {
      parameters['page'] = '$page';
    }
    final response =
        await http.get(Uri.https('www.pornhub.com', '/video/search', parameters.isNotEmpty ? parameters : null));

    if (response.statusCode == 200) {
      //Getting the html document from the response
      var document = parser.parse(response.body);
      try {
        //Scraping the first article title
        var responseString1 = document.getElementsByClassName('videoPreviewBg');

        for (final element in responseString1) {
          try {
            final id = (element.attributes['href'])?.split('=')[1] ?? 'ND';
            final title = element.attributes['title'] ?? '';

            final imageChildren = element.children[0];
            final image = imageChildren.attributes['data-thumb_url'] ?? '';

            videos.add(Video(
              id: id,
              title: title,
              imageUrl: image,
            ));
          } catch (_) {}
        }

        return videos;
      } catch (e) {
        log('$e');
        return videos;
      }
    } else {
      return videos;
    }
  }

  static Future<Video> getVideo(Video video) async {
    final response = await http.get(Uri.https('www.pornhub.com', '/view_video.php', {'viewkey': video.id}));

    if (response.statusCode == 200) {
      //Getting the html document from the response
      var document = parser.parse(response.body);
      try {
        //Scraping the first article title

        var videoParentWapper2 = document.getElementsByClassName('mainPlayerDiv');
        if (videoParentWapper2.isNotEmpty) {
          final divMainPlayer = videoParentWapper2.first;
          final scriptObject = divMainPlayer.nodes[1];
          final scriptString = scriptObject.nodes[0].toString();

          // String processed =
          // scriptString.replaceAll('\n', ' ');
          // processed = processed.replaceAll('\t', ' ');
          // processed = processed.substring(6, processed.length - 1);

          String processed = scriptString.substring(6, scriptString.length - 1);

          final variable = processed.substring(4, processed.indexOf('='));

          final script = 'function cosa(){ let playerObjList= {} \n $processed return $variable; } cosa()';

          final JavascriptRuntime js = getJavascriptRuntime();
          final response = js.evaluate(script);

          final data = js.convertValue(response);
          final sourceVideo = data['mediaDefinitions'][0]['videoUrl'];
          video = video.copyWith(videoUrl: sourceVideo);
        }

        return video;
      } catch (e) {
        log('$e');
        return video;
      }
    } else {
      return video;
    }
  }
}
